//Найти кол-во повторений каждого слова в тексте

import java.util.HashMap;
import java.util.Map;

public class Task5 {
    public static void main(String[] args) {
        long start_time = System.currentTimeMillis();
        String test_text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ";
        System.out.println(test_text);

        String[] splitted_text = test_text.split("\\W+"); //разбиваем текст на слова

        Map<String, Integer> unique =  new HashMap<String, Integer>();
        for(String words : splitted_text){
            Integer old_count = unique.get(words);
            if(old_count==null){
                old_count = 0;
            }
            unique.put(words, old_count + 1);
        }
        System.out.println(unique);
        long end_time = System.currentTimeMillis();
        System.out.println((end_time - start_time) / 1000d + "sec");
    }
}
