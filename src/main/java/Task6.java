//Найти все слова максимальной и минимальной длинны

import java.util.ArrayList;

public class Task6 {
    public static void main(String[] args) {
        long start_time = System.currentTimeMillis();
        String test_text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ";
        System.out.println(test_text);
        String[] splitted_text = test_text.split("\\W+"); //разбиваем текст на слова

        ArrayList<String> shortest_words = new ArrayList<String>();
        ArrayList<String> longest_words = new ArrayList<String>();
        shortest_words.add(splitted_text[0]);
        longest_words.add(splitted_text[0]);

        int min_length = splitted_text[0].length();
        int max_length = splitted_text[0].length();

        for(String word : splitted_text){
            if(min_length > word.length()){
                min_length = word.length();
                shortest_words.clear();
                shortest_words.add(word);
            }
            else if(min_length == word.length()){
                shortest_words.add(word);
            }

            if(max_length < word.length()){
                max_length = word.length();
                longest_words.clear();
                longest_words.add(word);
            }
            else if(max_length == word.length()){
                longest_words.add(word);
            }
        }

        System.out.println("Shortest: " + shortest_words);
        System.out.println("Longest: " + longest_words);
        long end_time = System.currentTimeMillis();
        System.out.println((end_time - start_time) / 1000d + "sec");
    }
}
