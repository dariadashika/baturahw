//Найти кол-во слов, которые начинаются и заканчиваются одинаковой буквой.

public class Task9 {
    public static void main(String[] args) {
        long start_time = System.currentTimeMillis();
        String test_text = "aaada rfxfb? hgfxxb fhfyf hytfujiyh"; //4 слова
        System.out.println(test_text);
        String[] splitted_text = test_text.split("\\W+"); //разбиваем текст на слова
        int count = 0;
        for(String words: splitted_text){
            if(words.charAt(0) == words.charAt(words.length()-1)){
                count++;
            }
        }
        System.out.println(count + " times");

        long end_time = System.currentTimeMillis();
        System.out.println((end_time - start_time) / 1000d + "sec");
    }
}
