import java.util.regex.Matcher;
import java.util.regex.Pattern;

//Найти кол-во гласных и согласных букв в тексте.
public class Task3 {
    public static void main(String[] args) {
        long start_time = System.currentTimeMillis();
        String test_text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ";
        System.out.println(test_text);
        Pattern vowels = Pattern.compile("(?iu)[aeiouy]");
        Pattern consonants = Pattern.compile("(?iu)[bcdfghjklmnpqrstvwxz]");

        Matcher mat_vowels = vowels.matcher(test_text);
        int vowels_counter = 0;
        while(mat_vowels.find()){
            vowels_counter++;
        }

        Matcher mat_consonants = consonants.matcher(test_text);
        int consonants_counter = 0;
        while(mat_consonants.find()){
            consonants_counter++;
        }

        System.out.println("Vocal count = " + vowels_counter);
        System.out.println("Vocal count = " + consonants_counter);
        long end_time = System.currentTimeMillis();
        System.out.println((end_time - start_time) / 1000d + "sec");
    }
}
