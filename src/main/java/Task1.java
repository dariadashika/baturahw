//Во всех предложениях поменять местами первое и последнее слово.
public class Task1 {
    public static void main(String[] args) {
        long start_time = System.currentTimeMillis();
        String test_text = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ";
        System.out.println(test_text);
        String[] splitted_text = test_text.split("\\. "); //разбиваем текст на предложения

        System.out.println(splitted_text);
        for (int i = 0; i < splitted_text.length; i++) {
            int first_index = splitted_text[i].indexOf(' ');
            int last_index = splitted_text[i].lastIndexOf(' ');
            String firstWord = splitted_text[i].substring(0, first_index).trim();
            String lastWord = splitted_text[i].substring(last_index).trim();
            splitted_text[i] = splitted_text[i].substring(first_index, last_index).trim();
            splitted_text[i] = lastWord + " " + splitted_text[i] + " " + firstWord;
            System.out.println("'" + splitted_text[i] + "'");
        }
        long end_time = System.currentTimeMillis();
        System.out.println((end_time - start_time) / 1000d + "sec");
    }
}
