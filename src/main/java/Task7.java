//Удаление лишних пробелов.

public class Task7 {
    public static void main(String[] args) {
        long start_time = System.currentTimeMillis();
        String test_text = "   Lorem ipsum       dolor sit amet  , consectetur adipiscing elit , sed do eiusmod tempor       incididunt ut labore et dolore magna aliqua.   ";
        System.out.println(test_text);
        String new_text = test_text.replaceAll("\\s{2,}|\\s+(?=(?:[,.?!:;-]))", "");
        System.out.println(new_text);
        long end_time = System.currentTimeMillis();
        System.out.println((end_time - start_time) / 1000d + "sec");
    }

}
