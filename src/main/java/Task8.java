//Заменить рядом стоящие одинаковые символы на один символ
public class Task8 {
    public static void main(String[] args) {
        long start_time = System.currentTimeMillis();
        String test_text = "Llorem ipssum doolor sit amet,,, connnsectetur adipPPiscing elitt, sedd do eiusmod tempor incididunt uut laborre et dolorre maggna aliqua..";
        System.out.println(test_text);

        String new_text = test_text.replaceAll("(?ui)([a-zA-Z,.?!:;-])\\1+", "$1");
        System.out.println(new_text);

        long end_time = System.currentTimeMillis();
        System.out.println((end_time - start_time) / 1000d + "sec");
    }
}
