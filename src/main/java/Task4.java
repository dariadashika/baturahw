//Каждый n-й символ заменить заданным символом

import java.util.Scanner;

public class Task4 {
    public static void main(String[] args) {
        long start_time = System.currentTimeMillis();
        Scanner in = new Scanner(System.in);
        System.out.print("Input a number: ");
        int num = in.nextInt();

        StringBuilder test_text = new StringBuilder("Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum. ");
        System.out.println(test_text);

        StringBuilder new_text = new StringBuilder();

        for(int i = num-1; i < test_text.length(); i += num){
            new_text = test_text.replace(i, i+1, "&");
        }
        System.out.println(new_text);
        long end_time = System.currentTimeMillis();
        System.out.println((end_time - start_time) / 1000d + "sec");
    }
}
